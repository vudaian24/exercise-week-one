import PropTypes from 'prop-types';
import ListItem from '../ListItem';

function List(props) {
    return (
        <div className='w-full flex flex-col items-center justify-center lg:px-[65px] md:px-[45px] sm:px-[25px]'>
            <ul className='grid sm:grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3'>
                {
                    props.data.map((item, index) => {
                        return (
                            <ListItem data={item} key={index}/>
                        )
                    })
                }
            </ul>
        </div>
    )
};

List.propTypes = {
    data: PropTypes.array.isRequired
}

export default List;
