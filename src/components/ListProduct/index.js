import ItemProduct from "../ItemProduct";

const ListProduct = ({ dataProducts }) => {
    return (
        <div className="mt-[35px] w-full pb-4 pt-[1px] px-4 grid gap-4 grid-cols-1 lg:grid-cols-4 md:grid-cols-2 overflow-y-auto rounded border-b-[2px] border-solid border-[#5A5B6A3D]">
            {dataProducts.map((item, index) => (
                <ItemProduct key={index} product={item}/>
            ))}
        </div>
    )
}

export default ListProduct;