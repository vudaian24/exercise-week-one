const ItemProduct = ({ product }) => {
    return (
        <div className="w-full flex flex-col px-4 pt-4 pb-3 rounded-[8px] hover:shadow-item-product">
            <div className="flex justify-center">
                <img src={product.image} className="w-[227px] h-[224px] object-cover" alt="" />
            </div>
            <p className="text-[16px] font-[400] leading-[24px] text-[#19191D] mt-3 line-clamp-3 h-[74px]">{product.description}</p>
            <h3 className="text-[24px] font-[700] leading-[36px] text-[#000000] opacity-85 mt-2">${product.price}</h3>
            <p className="text-[16px] font-[400] leading-[20px] text-[#787885] mt-2 line-clamp-2 h-[41px]">{product.additional}</p>
            <div className="mt-[9px] flex items-center justify-between flex-1 gap-4 py-1">
                <div className="flex items-center justify-between">
                    <div className="flex items-center justify-center mr-2">
                        <img src='./icons/ic-rating-stars.svg' alt="star" className="mr-[3px]" />
                        <img src='./icons/ic-rating-stars.svg' alt="star" className="mr-[3px]" />
                        <img src='./icons/ic-rating-stars.svg' alt="star" className="mr-[3px]" />
                        <img src='./icons/ic-rating-stars.svg' alt="star" className="mr-[3px]" />
                        <img src='./icons/ic-rating-stars-half.svg' alt="star" className="mr-[3px]" />
                    </div>
                    <p className="text-[14px] leading-[22px] font-[500] text-[#5E6366]">{product.ratingStar}</p>
                </div>
                <button className="border-[1px] border-solid border-[#9DC2FF] rounded px-3 py-1 flex items-center">
                    <img src="./icons/ic-heart.svg" alt="" />
                    <p className="text-[14px] text-[#2264D1] font-[500] leading-[20px] ml-[5px]">Watch</p>
                </button>
            </div>
        </div>
    )
};

export default ItemProduct;