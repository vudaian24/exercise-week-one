import PropTypes from 'prop-types';

function Layout({children}) {
    return (
        <div className='flex flex-col w-screen h-screen overflow-y-auto'>
            {children}
        </div>
    )
};
Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;