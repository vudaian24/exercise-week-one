const initialState = {
    data: [],
    delivery: null,
    useless: null,
    condition: null,
    resultsort: [],
};

export const sortReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SORT":
            return {
                ...state,
                data: action.payload,
            };
        case "DELIVERY":
            return {
                ...state,
                delivery: action.payload,
            };
        case "USELESS":
            return {
                ...state,
                useless: action.payload,
            };
        case "CONDITION":
            return {
                ...state,
                condition: action.payload,
            };
        case "RESULTSORT":
            return {
                ...state,
                resultsort: action.payload,
            };
        default:
            return state;
    }
};

export default sortReducer;
