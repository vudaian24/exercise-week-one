import { combineReducers, createStore } from "redux";
import activeReducer from "./active";
import sortReduer from "./sort";

const rootReducer = combineReducers({
  active: activeReducer,
  sort: sortReduer,
});

const store = createStore(rootReducer);

export default store;
