export const sortData = (dataProducts) => {
    return {
        type: "SORT",
        payload: dataProducts
    };
};

export const deliverySort = (delivery) => {
    return {
        type: "DELIVERY",
        payload: delivery
    }
};

export const uselessSort = (useless) => {
    return {
        type: "USELESS",
        payload: useless
    }
};

export const conditionSort = (condition) => {
    return {
        type: "CONDITION",
        payload: condition
    }
};

export const resultSort = (data) => {
    return {
        type: "RESULTSORT",
        payload: data
    }
};
