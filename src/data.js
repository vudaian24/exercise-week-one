const arrayData = [
    {
        icon: "./icons/ic-prison.svg",
        title: "4pt grid system",
        description: "Base on 4pt grid system. Our UI kit help you create perfect white space"
    },
    {
        icon: "./icons/ic-dropper.svg",
        title: "Color style",
        description: "All color in OpenArt are styled. You can change all design color with one click"
    },
    {
        icon: "./icons/ic-text_color.svg",
        title: "Free font",
        description: "OpenArt use Epilogue font family Available with open licence in gooogle font"
    },
    {
        icon: "./icons/ic-toggle_on.svg",
        title: "Darkmode avaiable",
        description: "Our UI Kit support darkmode Chage your design to dark mode with one click"
    },
    {
        icon: "./icons/ic-design.svg",
        title: "Easy to customize",
        description: "Create any design with OpenArt UI kits"
    },
    {
        icon: "./icons/ic-four_squares.svg",
        title: "Variant components",
        description: "All component art variant, easy to design, easy to control"
    }
];

const dataProductList = [
    {
        image: "./images/product1.png",
        description: "Vintage Typewriter to post awesome stories about UI design and webdev. Vintage Typewriter to post awesome stories about UI design and webdev.",
        price: "49.50",
        additional: "Eligible for Shipping To Mars or somewhere else",
        ratingStar: "4.05",
        delivery: 'Fast delivery',
        useless: 'Useless first',
        condition: 'New'
    },
    {
        image: "./images/product2.png",
        description: "Lee Pucker design. Leather botinki for handsome designers. Free shipping.",
        price: "13.95",
        additional: "1258 bids, 359 watchers $5.95 for shipping",
        ratingStar: "4.56",
        delivery: 'Express delivery',
        useless: 'Useless second',
        condition: 'New'
    },
    {
        image: "./images/product3.png",
        description: "Timesaving kitten to save months on development. Playful, cute, cheap!",
        price: "128.99",
        additional: "Wordwide shitting available Buyers protection possible!",
        ratingStar: "4.87",
        delivery: 'Fast delivery',
        useless: 'Useless second',
        condition: 'Old'
    },
    {
        image: "./images/product4.png",
        description: "Plastic useless plugs and tubes for high-fidelity prototyping. Fit & Eat!",
        price: "12.48",
        additional: "Wordwide shitting available Buyers protection possible!",
        ratingStar: "4.99",
        delivery: 'Fast delivery',
        useless: 'Useless first',
        condition: 'Old'
    },
    {
        image: "./images/product1.png",
        description: "Plastic useless plugs and tubes for high-fidelity prototyping. Fit & Eat!",
        price: "12.48",
        additional: "Wordwide shitting available Buyers protection possible!",
        ratingStar: "4.99",
        delivery: 'Express delivery',
        useless: 'Useless second',
        condition: 'New'
    },
    {
        image: "./images/product2.png",
        description: "Modern Wireless Keyboard and Mouse Combo. Sleek and stylish design.",
        price: "34.99",
        additional: "Connects seamlessly to all devices.",
        ratingStar: "4.25",
        delivery: 'Free shipping',
        useless: 'Useless first',
        condition: 'Old'
    },
    {
        image: "./images/product3.png",
        description: "Handcrafted Wooden Sunglasses. Eco-friendly and UV protected.",
        price: "59.95",
        additional: "Limited edition, engraved logo.",
        ratingStar: "4.65",
        delivery: 'Express delivery',
        useless: 'Useless second',
        condition: 'New'
    },
    {
        image: "./images/product4.png",
        description: "Smart Home Security Camera. Monitor your home remotely.",
        price: "89.99",
        additional: "Night vision, motion detection.",
        ratingStar: "4.75",
        delivery: 'Fast delivery',
        useless: 'Useless first',
        condition: 'Old'
    },
];


const products = [
    {
        name: 'Sản phẩm 1',
        price: '$6,000',
        quantity: '1',
        description: 'Lorem ipsum dolor sit amet',
        image: '../images/product-table-1.png',
    },
    {
        name: 'Sản phẩm 2',
        price: '$5,000',
        quantity: '3',
        description: 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet',
        image: '../images/product-table-2.png',
    },
    {
        name: 'Sản phẩm 3',
        price: '$40,000',
        quantity: '6',
        description: 'Lorem ipsum dolor sit amet',
        image: '../images/product-table-3.png',
    },
];

const users = [
    {
        avatar: '../images/user1.png',
        name: 'Dianne Russell',
        email: 'nevaeh.simmons@example.com',
        birthday: '1989/04/06',
        numberPhone: '063-222-1125',
    },
    {
        avatar: '../images/user2.png',
        name: 'Leslie Alexander',
        email: 'curtis.weaver@example.com',
        birthday: '1976/09/12',
        numberPhone: '088-124-1555',
    },
    {
        avatar: '../images/user3.png',
        name: 'Wade Warren',
        email: 'debbie.baker@example.com',
        birthday: '1954/02/08',
        numberPhone: '063-137-3355',
    },
];

export { arrayData, dataProductList, products, users };