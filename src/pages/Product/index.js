import Header from "../../components/Header";
import ListProduct from "../../components/ListProduct";
import TypeProduct from "../../components/TypeProduct";
import { dataProductList } from "../../data";
import { sortData } from "../../actions/sortData";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

function Product() {

    const dispatch = useDispatch();
    const dataProducts = useSelector((state) => state.sort.data);

    useEffect(() => {
        dispatch(sortData(dataProductList));
    }, [dispatch]);
    
    const listButton = [
        'worldwide shipping',
        'under $50',
        'kitten',
        'plastic plugs',
        'pucker shoes',
        'vintage typewriter'
    ];

    const listBanner = [
        {
            image: './images/banner1.png',
            title: 'Clear & Usable user flows',
            description: 'Let’s boost yout marketplace'
        },
        {
            image: './images/banner2.png',
            title: 'Clear & Usable user flows',
            description: 'Let’s boost yout marketplace'
        },
        {
            image: './images/banner3.png',
            title: 'Clear & Usable user flows',
            description: 'Let’s boost yout marketplace'
        }
    ];

    return (
        <div className="w-full flex flex-col">
            <Header />
            <TypeProduct listButton={listButton} />
            <ListProduct dataProducts={dataProducts} />
            <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-4 mt-10 mb-20">
                {
                    listBanner.map((item, index) => {
                        return (
                            <div key={index} className="rounded-[8px] bg-[#EDEDF0] flex flex-col items-center pt-[26px] pb-[24px]">
                                <div className="w-20 h-20 flex items-center justify-center">
                                    <img src={item.image} alt=""/>
                                </div>
                                <h2 className="text-[20px] font-[700] leading-[30px] text-[#1F1F1F]">{item.title}</h2>
                                <p className="text-[14px] leading-[20px] text-[#5A5B6A]">{item.description}</p>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    );
};

export default Product;