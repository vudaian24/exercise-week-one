import List from "../../components/List";
import {arrayData} from "../../data";

function HomePage() {

    // const [arrayData, setCards] = useState([]);

    // useEffect(() => {
    //     fetch('http://localhost:5000/api/cards')
    //         .then(res => res.json())
    //         .then(data => {
    //             setCards(data);
    //         })
    //         .catch(err => console.error(err))
    // }, [])

    return (
        <div className="w-full h-full custome_background overflow-y-auto">
            <div className="w-full lg:py-[90px] md:py-[60px] sm:py-[30px]">
                <h1 className="text-center text-black lg:text-[56px] md:text-[45px] sm:text-[40px] font-bold leading-[64px]">Inside Open Art</h1>
            </div>
            <List data={arrayData} />
        </div>
    )
}

export default HomePage;