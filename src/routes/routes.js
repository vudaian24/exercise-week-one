import config from '../config/index.js';

import Home from '../pages/Home';
import Product from './../pages/Product';
import ProductManager from './../pages/Managers/Product';
import UserManager from '../pages/Managers/User';
import LoginManager from '../pages/Managers/Login';

const publicRoutes = [
    { path: config.routes.home, component: Home },
    { path: config.routes.product, component: Product },
    { path: config.routes.productManager, component: ProductManager },
    { path: config.routes.usertManager, component: UserManager },
    { path: config.routes.loginManager, component: LoginManager },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };