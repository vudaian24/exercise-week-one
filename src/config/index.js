const routes = {
    home: '/',
    product: '/product',
    productManager: '/manager/product',
    usertManager: '/manager/user',
    loginManager: '/manager/login',
};
const config = {
    routes,
};

export default config;